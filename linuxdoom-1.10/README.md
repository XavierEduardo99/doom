# DOOM's original source code

This repository contains a very faithful fork of the DOOM source code provided by ID Software. Although the goal is to include some comforts in the building process (And perhaps in making it able to work with current GNU/Linux distributions).

### Compilation dependencies:
|Name|Package|
|---|---|
|GNU C Compiler|gcc|
|GNU Make|make|
|GIT|git|
|LIBC|libc6-dev|

If you are running any Debian based distribution, you'll need to execute the following command to install all the necesary dependencies

```bash
sudo apt install build-essential xserver-xephyr -y
```

After this, you'll want to clone this GitLab repository by running the following command:

```bash
git clone https://gitlab.com/XavierEduardo99/doom.git
```

Once you've opened the repository with your favorite editor, compile the code by running the following command: 
```bash
make
```

After this, you will notice that 2 directories where created. ```obj``` and ```build```. The first one just contains compilation objects; but the second one contains the resulting executable (Named ```doom```). After this, I suggest you place any vanilla WAD in the following directory:
```bash 
$HOME/.local/share/doom
```
Keep in mind that the executable will only recognize the following WAD names:
|WAD|Edition|
|---|---|
|doom2.wad|Commercial DOOM 2 release|
|doomu.wad|Retail version of Ultimate DOOM|
|doom.wad|Registered version of DOOM|
|doom1.wad|Shareware version of DOOM|
|plutonia.wad|Final DOOM - The Plutonia Experiment|
|tnt.wad|Final DOOM - TNT Evilution|
|doom2f.wad|DOOM 2 (French Version)|

After you've successfully copied your WAD file to the specified directory, you will need to start an X server in 8-bit 256-color mode (Pseudocolor), use Xephyr, which is an X server that outputs to a window on a pre-existing 'host' X display. The following command will do this: 

```bash
Xephyr :2 -ac -screen 640x480x8
```

Keep in mind that this command will keep your current terminal window busy, so open a new one (Or a new) tab, and type the following command:
```bash
DISPLAY=:2
```

And finally, just execute the game with the following command: 
```bash 
./build/doom -2
```
